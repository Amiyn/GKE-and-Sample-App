You can find my application at: http://35.197.145.224/
It runs on this GKE Cluster:

gcloud container clusters get-credentials multitierapp --zone asia-southeast1-a --project tiergitlabFetching cluster endpoint and auth data.kubeconfig entry generated for multitierapp.amiyn_al_ansare@cloudshell:~ (tiergitlab)$ kubectl get podsNAME                        READY   STATUS    RESTARTS   AGEmysql-6cbcb79fc4-pn92d      1/1     Running   0          15htest-app-5d9b66cb4c-f46h2   1/1     Running   0          15hamiyn_al_ansare@cloudshell:~ (tiergitlab)$ kubectl get servicesNAME            TYPE           CLUSTER-IP     EXTERNAL-IP                     PORT(S)        AGEkubernetes      ClusterIP      10.120.0.1     <none>                          443/TCP        15hmysql-service   ClusterIP      10.120.3.146   <none>                          3306/TCP       15htest-app        LoadBalancer   10.120.2.138   35.197.138.245,35.197.145.224   80:30814/TCP   15htest-service    LoadBalancer   10.120.6.233   35.197.145.224                  80:30522/TCP   15h


NAME                                          STATUS   ROLES    AGE   VERSION           INTERNAL-IP   EXTERNAL-IP      OS-IMAGE                             KERNEL-VERSION   CONTAINER-RUNTIME
gke-multitierapp-default-pool-2c3f649f-cbbs   Ready    <none>   38m   v1.13.11-gke.23   10.148.0.18   34.87.100.13     Container-Optimized OS from Google   4.14.138+        docker://18.9.7
gke-multitierapp-default-pool-2c3f649f-ncq3   Ready    <none>   37m   v1.13.11-gke.23   10.148.0.19   35.247.157.205   Container-Optimized OS from Google   4.14.138+        docker://18.9.7
gke-multitierapp-default-pool-2c3f649f-xxpq   Ready    <none>   37m   v1.13.11-gke.23   10.148.0.20   35.197.159.151   Container-Optimized OS from Google   4.14.138+        docker://18.9.7

I have published it like so:  
kubectl expose deployment my-deployment --name my-cip-service \
    --type ClusterIP --protocol TCP --port 80 --target-port 8080  



## How I deploy my application

1. Create services,  
    `kubectl create -f mysql-service.yaml`  
    `kubectl create -f testapp-service.yaml`

2. Create deployments,  
    `kubectl create -f mysql-deployment.yaml`  
    `kubectl create -f testapp-deployment.yaml`